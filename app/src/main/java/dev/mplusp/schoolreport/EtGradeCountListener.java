package dev.mplusp.schoolreport;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.SeekBar;

import java.util.Locale;

public class EtGradeCountListener implements TextWatcher {

    private static final String TAG = "EtGradeCountListener";

    private MainActivity mainActivity;
    private EditText editText;
    private SeekBar seekBar;

    EtGradeCountListener(MainActivity mainActivity, EditText editText, SeekBar seekBar) {
        this.mainActivity = mainActivity;
        this.editText = editText;
        this.seekBar = seekBar;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        // get EditText value as a string
        String valueString = s.toString();

        // handle empty string
        if (valueString.isEmpty()) {
            valueString = "";
        }

        // handle leading zeros
        if (valueString.length() > 1 && valueString.startsWith("0")) {
            valueString = valueString.substring(1);
            editText.setText(valueString);
        }

        // convert string to int value, handling unparseable strings
        int intValue = 0;
        try {
            intValue = Integer.parseInt(valueString);
        } catch (Exception e) {
            Log.e(TAG, "Error during Integer.parseInt(valueString).");
            editText.setText("0");
        }

        // handle values that are too big
        if (intValue > MainActivity.MAX_NUMBER_OF_MARKS) {
            intValue = MainActivity.MAX_NUMBER_OF_MARKS;
            editText.setText(String.format(Locale.getDefault(), "%d", intValue));
        }

        // place cursor at end of EditText
        editText.setSelection(editText.getText().length());

        // update seekBar
        seekBar.setProgress(intValue);

    }

    @Override
    public void afterTextChanged(Editable editable) {
        // update GradeManager data
        mainActivity.updateGradeManager();

        // update ui
        mainActivity.updateUi();
    }
}

package dev.mplusp.schoolreport;

import android.text.Editable;
import android.text.TextWatcher;

public class EtSubjectNameListener implements TextWatcher {
    private MainActivity mainActivity;

    EtSubjectNameListener(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (s.length() > 0) {
            mainActivity.setUiElementsEnabled(true);
        } else {
            mainActivity.setUiElementsEnabled(false);
        }

    }

    @Override
    public void afterTextChanged(Editable editable) { }
}

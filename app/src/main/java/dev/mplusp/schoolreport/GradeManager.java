package dev.mplusp.schoolreport;

import android.util.SparseIntArray;

class GradeManager {

    private SparseIntArray gradeCounts;

    GradeManager() {
        gradeCounts = new SparseIntArray();
    }

    void setGradeCount(int grade, int count) {
        gradeCounts.put(grade, count);
    }

    double calculateAverageGrade() {
        int gradeSum = sumGrades();
        int gradeCount = countGrades();

        if (gradeCount > 0) {
            return (double) gradeSum / gradeCount;
        } else {
            return 0.0;
        }
    }

    int calcNumberOfGradesToBetterAverage(int grade, double targetAverage) {
        int gradeSum = sumGrades();
        int gradeCount = countGrades();
        int additionalGradeCount = 0;
        double average = (double) gradeSum / gradeCount;

        // figure out how many better marks we need
        while (average >= targetAverage) {
            additionalGradeCount++;
            average = (gradeSum + additionalGradeCount * grade) / (double) (gradeCount + additionalGradeCount);
        }

        return additionalGradeCount;
    }


    private int countGrades() {
        int gradeCountSum = 0;

        for (int i = 0; i < gradeCounts.size(); i++) {
            gradeCountSum += gradeCounts.valueAt(i);
        }

        return gradeCountSum;
    }

    private int sumGrades() {
        int gradeSum = 0;

        for (int i = 0; i < gradeCounts.size(); i++) {
            gradeSum += gradeCounts.keyAt(i) * gradeCounts.valueAt(i);
        }

        return gradeSum;
    }

}

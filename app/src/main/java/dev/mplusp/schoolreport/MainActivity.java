package dev.mplusp.schoolreport;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    private static final String TAG = "MainActivity";

    public static final String SCHOOL_REPORT_DATA = "dev.mplusp.schoolreport.SCHOOL_REPORT_DATA";
    public static final String SCHOOL_REPORT_AVERAGE_GRADE = "dev.mplusp.schoolreport.SCHOOL_REPORT_AVERAGE_GRADE";
    public static final int MAX_NUMBER_OF_MARKS = 40;

    private EditText etSubjectName;

    private ArrayList<EditText> etGradeCounts;
    private ArrayList<SeekBar> sbGradeCounts;

    private Button btnAddToReport;

    private CheckBox cbComputeHintForImprovement;

    private TextView tvAverageValue;
    private TextView tvSubjectsAddedCount;
    private TextView tvHintForImprovement;

    private Drawable defaultButtonBackground;

    private boolean shouldUpdateHint = false;

    private GradeManager gradeManager;

    private Bundle schoolReportData;
    private double schoolReportAverageGrade;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // assign EditText attributes
        etSubjectName = findViewById(R.id.etSubjectName);
        etGradeCounts = new ArrayList<>();
        etGradeCounts.add((EditText) findViewById(R.id.etGradeCount1));
        etGradeCounts.add((EditText) findViewById(R.id.etGradeCount2));
        etGradeCounts.add((EditText) findViewById(R.id.etGradeCount3));
        etGradeCounts.add((EditText) findViewById(R.id.etGradeCount4));
        etGradeCounts.add((EditText) findViewById(R.id.etGradeCount5));
        etGradeCounts.add((EditText) findViewById(R.id.etGradeCount6));

        // assign SeekBar attributes
        sbGradeCounts = new ArrayList<>();
        sbGradeCounts.add((SeekBar) findViewById(R.id.sbGradeCount1));
        sbGradeCounts.add((SeekBar) findViewById(R.id.sbGradeCount2));
        sbGradeCounts.add((SeekBar) findViewById(R.id.sbGradeCount3));
        sbGradeCounts.add((SeekBar) findViewById(R.id.sbGradeCount4));
        sbGradeCounts.add((SeekBar) findViewById(R.id.sbGradeCount5));
        sbGradeCounts.add((SeekBar) findViewById(R.id.sbGradeCount6));

        // assign Button attributes
        btnAddToReport = findViewById(R.id.btnAddToReport);

        // assign CheckBox attributes
        cbComputeHintForImprovement = findViewById(R.id.cbComputeHintForImprovement);

        // assign TextView attributes
        tvAverageValue = findViewById(R.id.tvAverageValue);
        tvSubjectsAddedCount = findViewById(R.id.tvSubjectsAddedCount);
        tvHintForImprovement = findViewById(R.id.tvHintForImprovement);

        // create GradeManager
        gradeManager = new GradeManager();

        // create school report data Bundle
        schoolReportData = new Bundle();

        // save default button background so that we can restore it later
        defaultButtonBackground = btnAddToReport.getBackground();

        // add etSubjectName listener
        etSubjectName.addTextChangedListener(new EtSubjectNameListener(this));

        // add etGradeCount listeners
        for (int i = 0; i < etGradeCounts.size(); i++) {
            EditText editText = etGradeCounts.get(i);
            SeekBar seekBar = sbGradeCounts.get(i);
            editText.addTextChangedListener(new EtGradeCountListener(this, editText, seekBar));
        }

        // set SeekBar listeners
        for (int i = 0; i < sbGradeCounts.size(); i++) {
            SeekBar seekBar = sbGradeCounts.get(i);
            seekBar.setOnSeekBarChangeListener(this);
        }

        // set button listeners
        btnAddToReport.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addToReport();
            }
        });

        Button btnDeleteAllStartOver = findViewById(R.id.btnDeleteAllStartOver);
        btnDeleteAllStartOver.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startOver();
            }
        });

        FloatingActionButton btnShowSchoolReport = findViewById(R.id.btnShowSchoolReport);
        btnShowSchoolReport.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showSchoolReport();
            }
        });


        // set checkbox listener
        ((CheckBox) findViewById(R.id.cbComputeHintForImprovement)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setTvHintForImprovementVisibility(View.VISIBLE);
                    shouldUpdateHint = true;
                    updateUi();
                } else {
                    setTvHintForImprovementVisibility(View.INVISIBLE);
                    shouldUpdateHint = false;
                }
            }
        });

        // disable ui elements if no subject has been entered yet
        setUiElementsEnabled(false);

        // update ui here to get the number format in tvAverageValue correctly formatted depending on current locale
        updateUi();
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d(TAG, "onResume() got called");
        loadStateToSharedPrefs();
        updateUi();
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d(TAG, "onPause() got called");
        saveStateToSharedPrefs();
    }

    private void saveStateToSharedPrefs() {
        SharedPreferences.Editor editor = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE).edit();

        for (String subjectName : schoolReportData.keySet()) {
            editor.putInt(subjectName, schoolReportData.getInt(subjectName));
        }

        editor.apply();
    }

    private void loadStateToSharedPrefs() {
        SharedPreferences sharedPref = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        Log.d(TAG, "retrieving grades...");
        Map<String,?> sharedPrefKeys = sharedPref.getAll();

//        for (String subjectName : sharedPrefKeys.keySet()) {
//            Log.d(TAG, String.format("%s: %d", subjectName, sharedPref.getInt(subjectName, 0)));
//        }

        schoolReportData = new Bundle();

        for(Map.Entry<String,?> entry : sharedPrefKeys.entrySet()){
            schoolReportData.putInt(entry.getKey(), Integer.parseInt(entry.getValue().toString()));
            Log.d(TAG,entry.getKey() + ": " + entry.getValue().toString());
        }
        Log.d(TAG, "grades retrieved!");
    }

    private void clearStateFromSharedPreferences() {
        SharedPreferences.Editor editor = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE).edit();
        editor.clear().apply();
    }

    public void updateGradeManager() {
        for (int i = 0; i < etGradeCounts.size(); i ++) {
            gradeManager.setGradeCount(i + 1, getEditTextIntValue(etGradeCounts.get(i)));
        }
    }

    public void setUiElementsEnabled(boolean enabled) {
        setEditTextsEnabled(enabled);
        setSeekBarsEnabled(enabled);
        setBtnAddToReportEnabled(enabled);
        tryToActivateBtnAddToReport();
    }

    public void updateUi() {
        updateTvAverageValue();
        updateTvSubjectsAddedCount();
        updateAddToReportButtonBgColor();
        updateHint();
        tryToActivateBtnAddToReport();
    }

    private void updateTvSubjectsAddedCount() {
        tvSubjectsAddedCount.setText(getString(R.string.tv_subject_added_count_text, schoolReportData.size()));

    }

    private void tryToActivateBtnAddToReport() {
        // only activate add to report button, when subject name AND at least one grade have been entered
        if (etSubjectName.getText().length() > 0 && gradeManager.calculateAverageGrade() > 0.0) {
            btnAddToReport.setEnabled(true);
        } else {
            btnAddToReport.setEnabled(false);
        }
    }
    private void flushData() {
        gradeManager = new GradeManager();
        schoolReportData = new Bundle();
    }

    private void resetTvSubjectsAddedCount() {
        tvSubjectsAddedCount.setText(getString(R.string.tv_subject_added_count_default_text));
    }

    private void resetInputFields() {
        resetTextOnAllEditTexts();
        resetHint();
        setUiElementsEnabled(false);
    }

    private void startOver() {
        resetTextOnAllEditTexts();
        resetTvSubjectsAddedCount();
        resetHint();
        setUiElementsEnabled(false);
        flushData();
        clearStateFromSharedPreferences();

        Toast.makeText(getApplicationContext(), getString(R.string.btn_delete_all_start_over_toast_success_message), Toast.LENGTH_SHORT).show();
    }

    private void addToReport() {
        String subjectName = etSubjectName.getText().toString();

        if (schoolReportData.containsKey(subjectName)) {
            Toast.makeText(getApplicationContext(), getString(R.string.btn_add_to_report_toast_error_message), Toast.LENGTH_SHORT).show();
        } else {
            int grade = (int) Math.round(gradeManager.calculateAverageGrade());
            schoolReportData.putInt(subjectName, grade);
            updateTvSubjectsAddedCount();

            // calculate average grade for school report
            int gradeSum = 0;
            int subjectCount = schoolReportData.size();
            for (String subject : schoolReportData.keySet()) {
                gradeSum += schoolReportData.getInt(subject);
            }

            schoolReportAverageGrade = (double) gradeSum / subjectCount;

            Log.d(TAG, "schoolReportData Bundle: " + schoolReportData.toString());
            Log.d(TAG, "schoolReportAverageGrade: " + schoolReportAverageGrade);

            Toast.makeText(getApplicationContext(), getString(R.string.btn_add_to_report_toast_success_message), Toast.LENGTH_SHORT).show();

            resetInputFields();
        }
    }

    private void showSchoolReport() {
        if (schoolReportData.size() > 0) {
            Intent intent = new Intent(this, ShowSchoolReportActivity.class);
            intent.putExtra(SCHOOL_REPORT_DATA, schoolReportData);
            intent.putExtra(SCHOOL_REPORT_AVERAGE_GRADE, schoolReportAverageGrade);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.btn_show_school_report_toast_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void resetHint() {
        cbComputeHintForImprovement.setChecked(false);
    }

    private void resetTextOnAllEditTexts() {
        etSubjectName.setText("");
        for (int i = 0; i < etGradeCounts.size(); i++) {
            EditText editText = etGradeCounts.get(i);
            editText.setText("");
        }
    }

    private void setEditTextsEnabled(boolean enabled) {
        for (int i = 0; i < etGradeCounts.size(); i++) {
            EditText editText = etGradeCounts.get(i);
            editText.setEnabled(enabled);
        }
    }

    private void setSeekBarsEnabled(boolean enabled) {
        for (int i = 0; i < sbGradeCounts.size(); i++) {
            SeekBar seekBar = sbGradeCounts.get(i);
            seekBar.setEnabled(enabled);
        }
    }

    private void setBtnAddToReportEnabled(boolean enabled) {
        btnAddToReport.setEnabled(enabled);
    }

    private void setTvHintForImprovementVisibility(int visibility) {
        tvHintForImprovement.setVisibility(visibility);
    }

    private void updateHint() {
        if (shouldUpdateHint) {
            double averageValue = gradeManager.calculateAverageGrade();

            if (averageValue < 1.0) {
                tvHintForImprovement.setText(getString(R.string.tv_hint_for_improvement_default_text));
            }
            else if (averageValue < 1.5) {
                // you already have the best possible grade here
                tvHintForImprovement.setText(getString(R.string.tv_hint_for_improvement_grade_1_text));
            } else if (averageValue >= 1.5 && averageValue < 2.5) {
                double targetAverage = 1.5;
                int grade1 = 1;
                int additionalGradeCount1 = gradeManager.calcNumberOfGradesToBetterAverage(grade1, targetAverage);
                tvHintForImprovement.setText(getString(R.string.tv_hint_for_improvement_grade_2_text, additionalGradeCount1, grade1));
            } else if (averageValue >= 2.5 && averageValue < 3.5) {
                double targetAverage = 2.5;
                int grade1 = 1;
                int grade2 = 2;

                int additionalGradeCount1 = gradeManager.calcNumberOfGradesToBetterAverage(grade1, targetAverage);
                int additionalGradeCount2 = gradeManager.calcNumberOfGradesToBetterAverage(grade2, targetAverage);

                tvHintForImprovement.setText(getString(R.string.tv_hint_for_improvement_grade_3_text,
                        additionalGradeCount1, grade1,
                        additionalGradeCount2, grade2
                ));
            } else if (averageValue >= 3.5 && averageValue < 4.5) {
                double targetAverage = 3.5;
                int grade1 = 1;
                int grade2 = 2;
                int grade3 = 3;

                int additionalGradeCount1 = gradeManager.calcNumberOfGradesToBetterAverage(grade1, targetAverage);
                int additionalGradeCount2 = gradeManager.calcNumberOfGradesToBetterAverage(grade2, targetAverage);
                int additionalGradeCount3 = gradeManager.calcNumberOfGradesToBetterAverage(grade3, targetAverage);

                tvHintForImprovement.setText(getString(R.string.tv_hint_for_improvement_grade_4_text,
                        additionalGradeCount1, grade1,
                        additionalGradeCount2, grade2,
                        additionalGradeCount3, grade3
                ));
            } else if (averageValue >= 4.5 && averageValue < 5.5) {
                double targetAverage = 4.5;
                int grade1 = 1;
                int grade2 = 2;
                int grade3 = 3;
                int grade4 = 4;

                int additionalGradeCount1 = gradeManager.calcNumberOfGradesToBetterAverage(grade1, targetAverage);
                int additionalGradeCount2 = gradeManager.calcNumberOfGradesToBetterAverage(grade2, targetAverage);
                int additionalGradeCount3 = gradeManager.calcNumberOfGradesToBetterAverage(grade3, targetAverage);
                int additionalGradeCount4 = gradeManager.calcNumberOfGradesToBetterAverage(grade4, targetAverage);

                tvHintForImprovement.setText(getString(R.string.tv_hint_for_improvement_grade_5_text,
                        additionalGradeCount1, grade1,
                        additionalGradeCount2, grade2,
                        additionalGradeCount3, grade3,
                        additionalGradeCount4, grade4
                ));
            } else {
                double targetAverage = 5.5;
                int grade1 = 1;
                int grade2 = 2;
                int grade3 = 3;
                int grade4 = 4;
                int grade5 = 5;

                int additionalGradeCount1 = gradeManager.calcNumberOfGradesToBetterAverage(grade1, targetAverage);
                int additionalGradeCount2 = gradeManager.calcNumberOfGradesToBetterAverage(grade2, targetAverage);
                int additionalGradeCount3 = gradeManager.calcNumberOfGradesToBetterAverage(grade3, targetAverage);
                int additionalGradeCount4 = gradeManager.calcNumberOfGradesToBetterAverage(grade4, targetAverage);
                int additionalGradeCount5 = gradeManager.calcNumberOfGradesToBetterAverage(grade5, targetAverage);

                tvHintForImprovement.setText(getString(R.string.tv_hint_for_improvement_grade_6_text,
                        additionalGradeCount1, grade1,
                        additionalGradeCount2, grade2,
                        additionalGradeCount3, grade3,
                        additionalGradeCount4, grade4,
                        additionalGradeCount5, grade5
                ));
            }
        }
    }

    private void updateAddToReportButtonBgColor() {

        // gradually change the background color depending on averageGrade
        // from green (1.0) over yellow (3.0) to red (6.0)
        // red = Color.rgb(255, 0, 0);
        // yellow = Color.rgb(255, 255, 0);
        // green = Color.rgb(0, 255, 0);
        double averageGrade = gradeManager.calculateAverageGrade();

        // restore default button background when no marks are given
        if (averageGrade < 1.0) {
            btnAddToReport.setBackground(defaultButtonBackground);
        } else {
            double red;
            double green;
            double blue = 0.0;

            if (averageGrade <= 3.0) {
                red = 255 * averageGrade / 3;
                green = 255;
            } else {
                red = 255;
                green = 255 - (255 * averageGrade / 3);
            }
            btnAddToReport.setBackgroundColor(Color.rgb((int) red, (int) green, (int) blue));
        }
    }

    private void updateTvAverageValue() {
        DecimalFormat decimalFormat;

        double averageGrade = gradeManager.calculateAverageGrade();
        if (averageGrade > 0) {
            decimalFormat = new DecimalFormat("0.00");
            tvAverageValue.setText(decimalFormat.format(averageGrade));
        } else {
            decimalFormat = new DecimalFormat("0.0");
            tvAverageValue.setText(decimalFormat.format(0.0));
        }
    }

    private int getEditTextIntValue(EditText editText) {
        try {
            return Integer.parseInt(editText.getText().toString());
        } catch (Exception e) {
            return 0;
        }
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        EditText etGradeCount = null;

        // Get correct EditText
        switch (seekBar.getId()) {
            case R.id.sbGradeCount1:
                etGradeCount = etGradeCounts.get(0);
                break;

            case R.id.sbGradeCount2:
                etGradeCount = etGradeCounts.get(1);
                break;

            case R.id.sbGradeCount3:
                etGradeCount = etGradeCounts.get(2);
                break;

            case R.id.sbGradeCount4:
                etGradeCount = etGradeCounts.get(3);
                break;

            case R.id.sbGradeCount5:
                etGradeCount = etGradeCounts.get(4);
                break;

            case R.id.sbGradeCount6:
                etGradeCount = etGradeCounts.get(5);
                break;

            default:
                break;
        }

        // Actually set value
        if (etGradeCount != null) {
            etGradeCount.setText(String.format(Locale.getDefault(), "%d", progress));

        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}

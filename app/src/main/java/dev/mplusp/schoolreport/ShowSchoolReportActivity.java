package dev.mplusp.schoolreport;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class ShowSchoolReportActivity extends AppCompatActivity {

    private TextView tvSubjectNameList;
    private TextView tvSubjectGradeList;
    private Button btnDismiss;
    private boolean isActivityInFullScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_school_report);

        tvSubjectNameList = findViewById(R.id.tvSubjectNameList);
        tvSubjectGradeList = findViewById(R.id.tvSubjectGradeList);
        btnDismiss = findViewById(R.id.btnDismiss);

        // set listeners
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToMainActivity();
            }
        });

        View vOverlay = findViewById(R.id.vOverlay);
        vOverlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isActivityInFullScreen) disableFullScreen();
                else enableFullScreen();
            }
        });

        fillTvSubjectGradeList();
        fillTvSchoolReportAverageValue();
    }

        @Override
        protected void onResume() {
            super.onResume();

            if (getSupportActionBar() != null) {
                getSupportActionBar().hide();
            }

            enableFullScreen();
        }

    private void fillTvSubjectGradeList() {
        Bundle schoolReportData = getIntent().getBundleExtra(MainActivity.SCHOOL_REPORT_DATA);

        for (String subjectKey : schoolReportData.keySet()) {
            tvSubjectNameList.setText(String.format("%s%s\n", tvSubjectNameList.getText(), subjectKey));
            tvSubjectGradeList.setText(String.format("%s%s\n", tvSubjectGradeList.getText(), schoolReportData.get(subjectKey)));
        }
    }

    private void fillTvSchoolReportAverageValue() {
        TextView tvSchoolReportAverageValue = findViewById(R.id.tvSchoolReportAverageValue);
        DecimalFormat decimalFormat = new DecimalFormat("0.0");
        tvSchoolReportAverageValue.setText(decimalFormat.format(getIntent().getDoubleExtra(MainActivity.SCHOOL_REPORT_AVERAGE_GRADE, 0.0)));
    }

    private void backToMainActivity() {
        finish();
    }

    // disable back button functionality
    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(), getString(R.string.use_dismiss_button_toast_message_text), Toast.LENGTH_SHORT).show();
    }

    private void disableFullScreen() {
        showSystemUI();
        showDismissButton();
        isActivityInFullScreen = false;
    }

    private void enableFullScreen() {
        hideSystemUI();
        hideDismissButton();
        isActivityInFullScreen = true;
    }

    private void hideDismissButton() {
        btnDismiss.setVisibility(View.INVISIBLE);
    }

    private void showDismissButton() {
        btnDismiss.setVisibility(View.VISIBLE);
    }


    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
//                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
//                        | View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
    }

    // Shows the system bars by removing all the flags
// except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        );
    }
}
